/****************************************************************************
**
** SPDX-License-Identifier: <LICENSE_IDENTIFIER>
**
** SPDX-FileCopyrightText: Copyright (c) <CURRENT_YEAR> SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include <debug/sahtrace.h>
#include "utils.h"
#include "dm_wan_auto_sensing.h"
#include "wan_auto_sensing.h"
#include "wan_auto_sensing_netmodel.h"


typedef enum {
    DM_AS_Disabled,
    DM_AS_Error,
    DM_AS_Sensing,
    DM_AS_Enabled,
    DM_AS_Nr_
} dm_as_status_t;

static const char* dm_as_status_str[DM_AS_Nr_] = {
    [DM_AS_Disabled] = "Disabled",
    [DM_AS_Error] = "Error",
    [DM_AS_Sensing] = "Sensing",
    [DM_AS_Enabled] = "Enabled"
};
static amxd_object_t* wan_auto_sensing = NULL;
static wan_auto_sensing_app_t app;

static void wan_auto_sensing_update_disabled(void);
static int wan_mode_set_value(amxd_object_t* templ, amxd_object_t* instance, void* priv);
static const char* dm_as_status_to_str(int status);

const char* wan_auto_sensing_get_wan_manager_name(void) {
    return amxc_string_get(app.wan_manager_name, 0);
}

amxb_bus_ctx_t* wan_auto_sensing_get_ctx_wan_manager(void) {
    if(app.wan_manager_ctx == NULL) {
        app.wan_manager_ctx = amxb_be_who_has(wan_auto_sensing_get_wan_manager_name());
        when_null_trace(app.wan_manager_ctx, exit, ERROR, "Could not find the auto sensing bus");
    }
exit:
    return app.wan_manager_ctx;
}

amxd_dm_t* PRIVATE wan_auto_sensing_get_dm(void) {
    return app.dm;
}

amxo_parser_t* PRIVATE wan_auto_sensing_get_parser(void) {
    return app.parser;
}

amxd_object_t* wan_auto_sensing_get_root_instance(void) {
    if(wan_auto_sensing == NULL) {
        SAH_TRACEZ_ERROR(ME, "Asking for the root object, but it did not initialize yet");
    }
    return wan_auto_sensing;
}

const char* PRIVATE wan_auto_sensing_get_prefix(void) {
    amxc_var_t* setting = amxo_parser_get_config(wan_auto_sensing_get_parser(), "prefix_");
    return amxc_var_constcast(cstring_t, setting);
}

int _wan_auto_sensing_main(int reason,
                           amxd_dm_t* dm,
                           amxo_parser_t* parser) {
    int ret = -1;
    switch(reason) {
    case 0:
        app.dm = dm;
        app.parser = parser;
        wan_auto_sensing_init();
        when_failed(wan_auto_netmodel_init(), exit);
        wan_auto_netmodel_enable();
        break;
    case 1:
        wan_auto_netmodel_cleanup();
        wan_auto_sensing_cleanup();
        app.dm = NULL;
        app.parser = NULL;

        break;
    }
    ret = 0;
exit:
    return ret;
}

void wan_auto_sensing_init(void) {
    const char* prefix = wan_auto_sensing_get_prefix();
    when_null_trace(prefix, exit, ERROR, "Could not find the prefix");

    wan_auto_sensing = amxd_dm_findf(wan_auto_sensing_get_dm(), "%sWANAutoSensing.", prefix);
    when_null(wan_auto_sensing, exit);

    amxc_string_new(&app.wan_manager_name, 0);
    amxc_string_setf(app.wan_manager_name, "%sWANManager.", prefix);

    wan_auto_sensing_update_disabled();
exit:
    return;
}

void wan_auto_sensing_cleanup(void) {
    wan_auto_sensing = NULL;
    wan_auto_sensing_stop();
    if(NULL != app.wan_manager_name) {
        amxc_string_delete(&app.wan_manager_name);
    }
    app.wan_manager_ctx = NULL;
}

void wan_auto_sensing_set_state(const char* state, const char* enable_status) {
    amxd_object_t* wan_modes = amxd_object_get_child(wan_auto_sensing, "Detect");
    amxc_string_t spath;
    amxc_string_t* value = NULL;
    amxc_string_init(&spath, 0);

    when_null(wan_modes, exit);
    when_null(state, exit);
    when_null(enable_status, exit);

    amxc_string_new(&value, 0);
    amxc_string_set(value, state);
    amxc_string_setf(&spath, "[Enable==%s].", enable_status);
    amxd_object_for_all(wan_modes, amxc_string_get(&spath, 0), wan_mode_set_value, value);

exit:
    amxc_string_delete(&value);
    amxc_string_clean(&spath);
}

void _auto_sensing_toggle(UNUSED const char* const event_name,
                          const amxc_var_t* const event_data,
                          UNUSED void* const priv) {
    bool enable = GETP_BOOL(event_data, "parameters.Enable.to");

    dm_object_set_status(wan_auto_sensing, dm_as_status_to_str((enable ? DM_AS_Enabled : DM_AS_Disabled)));

    if(enable) {
        wan_auto_netmodel_enable();
    } else {
        wan_auto_netmodel_disable();
        wan_auto_sensing_set_state("NotChecked", "true");
    }
}

void _toggle_mode(UNUSED const char* const event_name,
                  const amxc_var_t* const event_data,
                  UNUSED void* const priv) {
    const char* object_path = GETP_CHAR(event_data, "path");
    bool enable = GETP_BOOL(event_data, "parameters.Enable.to");
    amxd_object_t* wan_mode = amxd_dm_findf(wan_auto_sensing_get_dm(), "%s", object_path);

    if(NULL != wan_mode) {
        dm_object_set_status(wan_mode, (enable ? "NotChecked" : "Disabled"));
    }
}


static void wan_auto_sensing_update_disabled(void) {
    wan_auto_sensing_set_state("Disabled", "false");
}

static int wan_mode_set_value(UNUSED amxd_object_t* templ, amxd_object_t* instance, void* priv) {
    amxc_string_t* value = (amxc_string_t*) priv;
    (void) dm_object_set_status(instance, amxc_string_get(value, 0));
    return 1;
}

static const char* dm_as_status_to_str(int status) {
    return (DM_AS_Disabled <= status && DM_AS_Nr_ > status) ? dm_as_status_str[status] : "Error";
}

void dm_object_set_status(amxd_object_t* object, const char* status) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_object(&trans, object);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxd_trans_set_value(cstring_t, &trans, "Status", status);
    if(amxd_trans_apply(&trans, wan_auto_sensing_get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to apply the transaction for setting the status %s", status);
    }

    amxd_trans_clean(&trans);
}
