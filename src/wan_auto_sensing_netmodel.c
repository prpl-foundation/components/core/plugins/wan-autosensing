/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_action.h>

#include "netmodel/client.h"
#include <debug/sahtrace.h>
#include "utils.h"
#include "netmodel/client.h"
#include "wan_auto_sensing_netmodel.h"
#include "wan_auto_sensing.h"
#include "dm_wan_auto_sensing.h"

/**********************************************************
* MACRO definitions
**********************************************************/

#define string_empty(x) ((x == NULL) || (*x == '\0'))
#define CONST_ETHERNET_INTERFACE "Device.IP.Interface.2."

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/

static netmodel_query_t* nm_query = NULL;

/**********************************************************
* Function Prototypes
**********************************************************/

static void autosensing_nm_netdevname_changed_cb(const char* const sig_name,
                                                 const amxc_var_t* const data,
                                                 void* const priv);

/**********************************************************
* Functions
**********************************************************/

void wan_auto_netmodel_enable(void) {
    amxd_status_t status = amxd_status_unknown_error;

    when_false(amxd_object_get_value(bool, wan_auto_sensing_get_root_instance(), "Enable", NULL), exit_ignore);
    when_false_trace(nm_query == NULL, exit, ERROR, "netmodel query should be empty");

    wan_auto_sensing_set_state("NotChecked", "true");
    nm_query = netmodel_openQuery_getFirstParameter(CONST_ETHERNET_INTERFACE, "wan_auto_sensing", "NetDevName", "eth_intf && netdev-up && upstream", netmodel_traverse_all, autosensing_nm_netdevname_changed_cb, NULL);
    if(nm_query == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to add a query to netmodel, make sure the interface param has a valid value");
    } else {
        status = amxd_status_ok;
        SAH_TRACEZ_INFO(ME, "Successfully added a dhcp query to netmodel, for the default route intf %s", CONST_ETHERNET_INTERFACE);
        if(wan_auto_sensing_in_progress()) {
            netmodel_closeQuery(nm_query);
            nm_query = NULL;
        }
    }
exit:
    if(status != amxd_status_ok) {
        dm_object_set_status(wan_auto_sensing_get_root_instance(), "Error");
    }
exit_ignore:
    return;
}

static void autosensing_nm_netdevname_changed_cb(UNUSED const char* const sig_name,
                                                 const amxc_var_t* const data,
                                                 UNUSED void* const priv) {
    amxd_status_t status = amxd_status_unknown_error;
    cstring_t return_value = NULL;

    when_null_trace(data, exit, ERROR, "Invalid data parameter");
    return_value = amxc_var_dyncast(cstring_t, data);
    if(!string_empty(return_value)) {
        if(nm_query != NULL) {
            netmodel_closeQuery(nm_query);
            nm_query = NULL;
        }
        wan_auto_sensing_set_state("NotChecked", "true");
        when_failed_trace(wan_auto_sensing_refresh(wan_auto_sensing_get_root_instance()), exit, ERROR, "Failed to refresh auto sensing");
        wan_auto_sensing_start();
        status = amxd_status_ok;
    }
exit:
    dm_object_set_status(wan_auto_sensing_get_root_instance(), status == amxd_status_ok ? "Enabled" : "Error");
    free(return_value);
}

void wan_auto_netmodel_disable(void) {
    if(nm_query != NULL) {
        netmodel_closeQuery(nm_query);
        nm_query = NULL;
    }
}

amxd_status_t wan_auto_netmodel_init(void) {
    amxd_status_t status = amxd_status_unknown_error;

    when_false_trace(netmodel_initialize() == 1, exit, ERROR, "Failed to init netmodel");
    status = amxd_status_ok;
exit:
    return status;
}

void wan_auto_netmodel_cleanup(void) {
    wan_auto_netmodel_disable();
    netmodel_cleanup();
}