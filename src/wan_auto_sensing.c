/****************************************************************************
**
** SPDX-License-Identifier: <LICENSE_IDENTIFIER>
**
** SPDX-FileCopyrightText: Copyright (c) <CURRENT_YEAR> SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include <debug/sahtrace.h>
#include "dm_wan_auto_sensing.h"
#include "utils.h"
#include "wan_auto_sensing.h"

#define string_empty(x) ((x == NULL) || (*x == '\0'))
#define S_TO_MS_FACTOR (1000u)

typedef struct {
    amxc_llist_it_t* current_mode;
    amxd_object_t* wan_auto_sensing;
    amxd_object_t* instance;
    uint32_t checked;
    uint32_t total;
    amxp_timer_t* timer;
    bool in_progress;
} auto_sensing_t;

typedef enum {
    AutoSensing_Disabled,
    AutoSensing_Pass,
    AutoSensing_Error,
    AutoSensing_Pending,
    AutoSensing_NotChecked,
    AutoSensing_Nr_
} auto_sensing_status_t;

static const char* auto_sensing_status_str[AutoSensing_Nr_] = {
    [AutoSensing_Disabled] = "Disabled",
    [AutoSensing_Pass] = "Pass",
    [AutoSensing_Error] = "Error",
    [AutoSensing_Pending] = "Pending",
    [AutoSensing_NotChecked] = "NotChecked"
};

static auto_sensing_t auto_sensing = {
    .current_mode = NULL,
    .wan_auto_sensing = NULL,
    .instance = NULL,
    .checked = 0u,
    .total = 0u,
    .timer = NULL,
    .in_progress = false
};

static auto_sensing_status_t auto_sensing_status_from_string(const char* str);
static const char* auto_sensing_status_to_str(int status);
static amxd_status_t auto_sensing_set_wan_mode(const char* wan_mode);
static amxd_status_t auto_sensing_check_mode(amxd_object_t* mode);
static void auto_sensing_set_mode_status(amxd_object_t* mode, auto_sensing_status_t status);
static void auto_sensing_step(void);
static void auto_sensing_next_mode(void);
static auto_sensing_status_t auto_sensing_get_status(amxd_object_t* mode);
static bool auto_sensing_is_valid_mode(void);
static void auto_sensing_wait_for_mode(amxd_object_t* mode);
static void auto_sensing_timer_cb(amxp_timer_t* const timer, void* data);
static void auto_sensing_set_str_parameter(const char* parameter, const char* value);
static void auto_sensing_continuous(void);
static amxc_var_t* auto_sensing_call_wan_manager_function(amxc_var_t* args, const char* function);
static bool auto_sensing_get_current_wan_mode_status(void);
static amxc_llist_it_t* auto_sensing_get_last_wan_mode(void);

amxd_status_t wan_auto_sensing_refresh(amxd_object_t* root) {
    amxd_status_t rc = amxd_status_unknown_error;
    when_null_l(root, exit, "root object is NULL");
    const char* prefix = NULL;

    auto_sensing.instance = amxd_object_get_child(root, "Detect");
    when_null(auto_sensing.instance, exit);

    auto_sensing.total = amxd_object_get_instance_count(auto_sensing.instance);
    auto_sensing.checked = 0u;
    auto_sensing.current_mode = NULL;

    if(NULL == auto_sensing.wan_auto_sensing) {
        prefix = wan_auto_sensing_get_prefix();
        when_null_trace(prefix, exit, ERROR, "Failed to get the prefix");

        auto_sensing.wan_auto_sensing = amxd_dm_findf(wan_auto_sensing_get_dm(), "%sWANAutoSensing", prefix);
        when_null_trace(auto_sensing.wan_auto_sensing, exit, ERROR, "Could not find the auto sensing instance");
    }

    rc = amxd_status_ok;

    if(NULL == auto_sensing.timer) {
        amxp_timer_new(&auto_sensing.timer, auto_sensing_timer_cb, NULL);
    }

    SAH_TRACEZ_INFO(ME, "%d modes to check", auto_sensing.total);
    if(0u != auto_sensing.total) {
        auto_sensing_next_mode();
    }
exit:
    return rc;
}

bool wan_auto_sensing_in_progress(void) {
    return auto_sensing.in_progress;
}

void wan_auto_sensing_start(void) {
    if(NULL != auto_sensing.instance) {
        SAH_TRACEZ_INFO(ME, "Start initial sensing cycle");
        auto_sensing.in_progress = true;
        auto_sensing_step();
    }
}

void wan_auto_sensing_stop(void) {
    if(NULL != auto_sensing.timer) {
        amxp_timer_delete(&auto_sensing.timer);
    }
}

static amxd_status_t auto_sensing_check_mode(amxd_object_t* mode) {
    amxd_status_t status = amxd_status_unknown_error;
    char* wan_mode = NULL;

    when_null(mode, exit);
    wan_mode = amxd_object_get_value(cstring_t, mode, "WANMode", NULL);
    SAH_TRACEZ_INFO(ME, "Try to set %s WANMode", wan_mode);
    status = auto_sensing_set_wan_mode(wan_mode);
    auto_sensing_set_mode_status(mode, (status == amxd_status_ok) ? AutoSensing_Pending : AutoSensing_Error);

exit:
    free(wan_mode);
    return status;
}

static auto_sensing_status_t auto_sensing_status_from_string(const char* str) {
    auto_sensing_status_t status = AutoSensing_Disabled;

    when_str_empty(str, exit);
    for(int i = 0; i < AutoSensing_Nr_; ++i) {
        if(0 == strcmp(str, auto_sensing_status_str[i])) {
            status = (auto_sensing_status_t) i;
            goto exit;
        }
    }

exit:
    return status;
}

static const char* auto_sensing_status_to_str(int status) {
    return (AutoSensing_Disabled <= status && AutoSensing_Nr_ > status ) ? auto_sensing_status_str[status] : "Disabled";
}

static amxd_status_t auto_sensing_set_wan_mode(const char* wan_mode) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t args;
    amxc_var_t* ret = NULL;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    when_str_empty(wan_mode, exit);

    auto_sensing_set_str_parameter("ActiveWANMode", wan_mode);
    amxc_var_add_key(cstring_t, &args, "WANMode", wan_mode);
    amxc_var_add_key(bool, &args, "Autosensing", true);
    ret = auto_sensing_call_wan_manager_function(&args, "setWANMode");
    when_null(ret, exit);
    amxc_llist_it_t* it = amxc_llist_get_first(amxc_var_get_const_amxc_llist_t(ret));
    when_null(it, exit);
    amxc_var_t* mvar = amxc_container_of(it, amxc_var_t, lit);

    status = (GETP_BOOL(mvar, "status") == false  ? amxd_status_unknown_error : amxd_status_ok);

exit:
    amxc_var_clean(&args);
    amxc_var_delete(&ret);
    return status;
}

static bool auto_sensing_is_valid_mode(void) {
    amxd_trans_t trans;
    bool is_detected = auto_sensing_get_current_wan_mode_status();

    if(is_detected) {
        amxd_object_t* mode = amxc_container_of(auto_sensing.current_mode, amxd_object_t, it);
        char* value = amxd_object_get_value(cstring_t, mode, "WANMode", NULL);
        amxd_trans_init(&trans);
        amxd_trans_select_object(&trans, auto_sensing.wan_auto_sensing);
        amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
        amxd_trans_set_value(cstring_t, &trans, "LastWANMode", value);
        if(amxd_trans_apply(&trans, wan_auto_sensing_get_dm()) != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to apply the transaction for setting the lastWANMode %s", value);
        }
        amxd_trans_clean(&trans);
        free(value);
    }

    return is_detected;
}

static bool auto_sensing_get_current_wan_mode_status(void) {
    amxc_var_t* ret = auto_sensing_call_wan_manager_function(NULL, "getCurrentWANModeStatus");
    bool status = false;

    when_null(ret, exit);
    amxc_llist_it_t* it = amxc_llist_get_first(amxc_var_get_const_amxc_llist_t(ret));
    when_null(it, exit);
    amxc_var_t* mvar = amxc_container_of(it, amxc_var_t, lit);

    status = GETP_BOOL(mvar, "status");

exit:
    amxc_var_delete(&ret);
    return status;
}

static amxc_var_t* auto_sensing_call_wan_manager_function(amxc_var_t* args, const char* function) {
    amxc_var_t* ret = NULL;

    when_str_empty(function, exit);
    amxc_var_new(&ret);

    if(AMXB_STATUS_OK != amxb_call(wan_auto_sensing_get_ctx_wan_manager(),
                                   wan_auto_sensing_get_wan_manager_name(),
                                   function,
                                   args,
                                   ret,
                                   5)) {
        SAH_TRACEZ_ERROR(ME, "%s invoke failed", function);
        amxc_var_delete(&ret);
    }

exit:
    return ret;
}

static void auto_sensing_set_mode_status(amxd_object_t* mode, auto_sensing_status_t status) {
    if(NULL != mode) {
        amxd_object_set_value(cstring_t, mode, "Status", auto_sensing_status_to_str(status));
    }
}

static void auto_sensing_step(void) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* mode = NULL;

    when_null_l(auto_sensing.current_mode, exit, "no valid mode to check");
    mode = amxc_container_of(auto_sensing.current_mode, amxd_object_t, it);
    status = auto_sensing_check_mode(mode);

    if(amxd_status_ok != status) {
        char* wan_mode = amxd_object_get_value(cstring_t, mode, "WANMode", NULL);
        SAH_TRACEZ_ERROR(ME, "Cannot set WANMode %s", wan_mode);
        free(wan_mode);
    }
    auto_sensing_wait_for_mode(mode);
exit:
    return;
}

static amxc_llist_it_t* auto_sensing_get_last_wan_mode(void) {
    cstring_t last_used_wanmode = NULL;
    cstring_t wan_mode = NULL;
    amxc_llist_it_t* next_mode = NULL;
    amxd_object_t* next_mode_inst = NULL;

    when_null_trace(auto_sensing.wan_auto_sensing, exit, ERROR, "Could not find the root auto_sensing instance");

    last_used_wanmode = amxd_object_get_value(cstring_t, auto_sensing.wan_auto_sensing, "LastWANMode", NULL);
    if(!string_empty(last_used_wanmode)) {
        next_mode = amxd_object_first_instance(auto_sensing.instance);
        while(next_mode != NULL) {
            next_mode_inst = amxc_container_of(next_mode, amxd_object_t, it);
            when_null_trace(next_mode_inst, exit, ERROR, "Could not find the object in the list");

            wan_mode = amxd_object_get_value(cstring_t, next_mode_inst, "WANMode", NULL);
            when_null_trace(wan_mode, exit, ERROR, "Failed to get the WANMode parameter");
            if(strcmp(last_used_wanmode, wan_mode) == 0) {
                free(wan_mode);
                break;
            }

            free(wan_mode);
            wan_mode = NULL;
            next_mode = amxc_llist_it_get_next(next_mode);
        }
    }
exit:
    free(last_used_wanmode);
    return next_mode;
}

static void auto_sensing_next_mode(void) {
    amxd_object_t* mode_inst = NULL;

    if(NULL == auto_sensing.current_mode) {
        auto_sensing.current_mode = auto_sensing_get_last_wan_mode();
    }

    while(auto_sensing.checked != auto_sensing.total) {
        if(NULL == auto_sensing.current_mode) {
            auto_sensing.current_mode = amxd_object_first_instance(auto_sensing.instance);
        }
        when_null_trace(auto_sensing.current_mode, exit, ERROR, "No mode where found");
        mode_inst = amxc_container_of(auto_sensing.current_mode, amxd_object_t, it);
        when_null_trace(mode_inst, exit, ERROR, "Unexpected error. Mode instance should never be NULL");

        if(AutoSensing_NotChecked == auto_sensing_get_status(mode_inst)) {
            goto exit;
        }
        auto_sensing.checked++;
        auto_sensing.current_mode = amxc_llist_it_get_next(auto_sensing.current_mode);
    }

    auto_sensing.in_progress = false;
    auto_sensing.current_mode = NULL;
    auto_sensing_continuous();
exit:
    return;
}

static auto_sensing_status_t auto_sensing_get_status(amxd_object_t* mode) {
    char* str = amxd_object_get_value(cstring_t, mode, "Status", NULL);
    auto_sensing_status_t status = auto_sensing_status_from_string(str);
    free(str);
    return status;
}

static void auto_sensing_wait_for_mode(amxd_object_t* mode) {
    uint32_t timeout = amxd_object_get_value(uint32_t, mode, "Timeout", NULL) * S_TO_MS_FACTOR;
    SAH_TRACEZ_INFO(ME, "Wait for WANMode timeout %d", timeout);
    if(amxp_timer_running == amxp_timer_get_state(auto_sensing.timer)) {
        amxp_timer_stop(auto_sensing.timer);
    }
    amxp_timer_start(auto_sensing.timer, timeout);
}

static void auto_sensing_timer_cb(UNUSED amxp_timer_t* const timer, UNUSED void* data) {
    amxd_object_t* mode = amxc_container_of(auto_sensing.current_mode, amxd_object_t, it);
    char* name = NULL;
    when_null_l(mode, exit, "Unexpected error. Mode cannot be null");
    name = amxd_object_get_value(cstring_t, mode, "WANMode", NULL);
    if(auto_sensing_is_valid_mode()) {
        SAH_TRACEZ_INFO(ME, "Detected %s WANMode", name);
        auto_sensing_set_mode_status(mode, AutoSensing_Pass);
        auto_sensing.in_progress = false;
    } else {
        SAH_TRACEZ_INFO(ME, "%s WANMode was not detected try next mode", name);
        auto_sensing_set_mode_status(mode, AutoSensing_Error);
        auto_sensing_next_mode();
        auto_sensing_step();
    }

exit:
    free(name);
}


static void auto_sensing_set_str_parameter(const char* parameter, const char* value) {
    when_str_empty(parameter, exit);
    when_str_empty(value, exit);
    when_null(auto_sensing.wan_auto_sensing, exit);

    (void) amxd_object_set_value(cstring_t, auto_sensing.wan_auto_sensing, parameter, value);

exit:
    return;
}

static void auto_sensing_continuous(void) {
    char* mode = NULL;
    amxd_object_t* config = amxd_object_get_child(auto_sensing.wan_auto_sensing, "Config");

    when_null(config, exit);
    mode = amxd_object_get_value(cstring_t, config, "Policy", NULL);
    if(0 == strcmp("Continuous", mode)) {
        SAH_TRACEZ_INFO(ME, "Continuous policy set. Start detection cycle");
        auto_sensing.checked = 0;
        auto_sensing.in_progress = true;
        wan_auto_sensing_set_state("NotChecked", "true");
        auto_sensing_next_mode();
    }

exit:
    free(mode);
    return;
}

