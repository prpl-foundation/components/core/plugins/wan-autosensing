# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.3.0 - 2023-01-05(11:52:51 +0000)

### New

- [import-dbg] Disable import-dbg by default for all amxrt plugin

## Release v0.2.9 - 2022-12-15(12:08:19 +0000)

### Changes

- [wan-autosensing] Need a transaction on LastWANMode parameter to get notified when changed

## Release v0.2.8 - 2022-12-09(09:31:51 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v0.2.7 - 2022-07-01(06:04:02 +0000)

### Fixes

- [wan-autosensing] errors when DHCPv6client does not start

## Release v0.2.6 - 2022-06-30(09:16:58 +0000)

### Fixes

- [wan-autosensing] fix startup

## Release v0.2.5 - 2022-05-23(07:41:33 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.2.4 - 2022-04-08(08:39:12 +0000)

### Changes

- Align defaults with latest WANManager

## Release v0.2.3 - 2022-03-24(09:58:36 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v0.2.2 - 2022-03-22(16:07:45 +0000)

### Changes

- [amx][WANAutosensing] [WANManager] Use proper vendor extension prefix

## Release v0.2.1 - 2022-02-25(11:12:29 +0000)

### Other

- Enable core dumps by default

## Release v0.2.0 - 2021-12-21(08:35:16 +0000)

### New

- Automatic interface sensing data model

## Release v0.1.0 - 2021-10-14(12:14:10 +0000)

### Other

- split autosensing and wanmanger in separate repos
