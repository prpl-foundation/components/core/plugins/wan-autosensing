#!/bin/sh
[ -f /etc/environment ] && source /etc/environment
ulimit -c ${ULIMIT_CONFIGURATION:-0}

case $1 in
    start|boot)
	amxrt -D /etc/amx/wan-autosensing/wan-autosensing.odl
        ;;
    stop)
        if [ -f /var/run/wan-autosensing.pid ]; then
            kill `cat /var/run/wan-autosensing.pid`
        fi
        ;;
    debuginfo)
	ubus-cli "X_PRPL-COM_WANAutoSensing.?"
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    log)
	echo "TODO log wan-autosensing client"
	;;
    *)
        echo "Usage : $0 [start|boot|stop|debuginfo|log]"
        ;;
esac
