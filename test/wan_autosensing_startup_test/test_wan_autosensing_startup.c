/****************************************************************************
**
** SPDX-License-Identifier: <LICENSE_IDENTIFIER>
**
** SPDX-FileCopyrightText: Copyright (c) <CURRENT_YEAR> SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_lqueue.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_dm.h>
#include <amxc/amxc_rbuffer.h>
#include <amxc/amxc_astack.h>
#include <amxc/amxc_lstack.h>
#include <amxo/amxo.h>
#include <amxp/amxp_slot.h>
#include <amxb/amxb.h>
#include <amxb/amxb_be.h>
#include <amxb/amxb_register.h>

#include "test_wan_autosensing_startup.h"
#include "../mocks/mock_netmodel.h"
#include "../mocks/dummy_backend.h"
#include "dm_wan_auto_sensing.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxp_signal_t* signal_handler = NULL;

static const char* odl_defs = "wan-autosensing_test.odl";
static const char* wan_manager_mock_odl = "wan-manager_mock.odl";
static int current_mode_index = -1;

static int test_wan_setup(const char* wan_manager_odl);
static void test_wan_autosensing_read_sigalrm(void);
static void wait_for_timer_event(void);
static void test_wan_autosensing_set_enable(bool state);
static void test_wan_autosensing_assert_status_equal(const char* to);
static void test_detect_assert_status_equal(const char* to, int index);

int __wrap_amxb_call(amxb_bus_ctx_t* const bus_ctx,
                     const char* object,
                     const char* method,
                     amxc_var_t* args,
                     amxc_var_t* ret,
                     int timeout);

int test_wan_autosensing_setup(UNUSED void** state) {
    return test_wan_setup(wan_manager_mock_odl);
}

int test_wan_autosensing_err_setup(UNUSED void** state) {
    return test_wan_setup(NULL);
}

int test_wan_autosensing_teardown(UNUSED void** state) {
    _wan_auto_sensing_main(1, &dm, &parser);
    amxo_resolver_import_close_all();
    assert_int_equal(test_unregister_dummy_be(), 0);

    amxp_signal_delete(&signal_handler);
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

amxd_dm_t* test_get_dm(void) {
    return &dm;
}

amxo_parser_t* test_get_parser(void) {
    return &parser;
}

const char* test_get_prefix(void) {
    amxc_var_t* setting = amxo_parser_get_config(&parser, "prefix_");
    return amxc_var_constcast(cstring_t, setting);
}

void test_handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

void test_wan_autosensing_should_be_disabled_by_default(UNUSED void** state) {
    test_wan_autosensing_assert_status_equal("Disabled");
}

void test_wan_autosensing_detect_vlan(UNUSED void** state) {
    current_mode_index = 1;
    amxc_var_t data_cb;

    amxc_var_init(&data_cb);

    test_detect_assert_status_equal("NotChecked", current_mode_index);
    test_wan_autosensing_set_enable(true);
    test_wan_autosensing_assert_status_equal("Enabled");

    amxc_var_set(cstring_t, &data_cb, "eth0");
    trigger_getFirstParameter_callback(&data_cb);

    test_detect_assert_status_equal("Pending", current_mode_index);
    wait_for_timer_event();
    test_detect_assert_status_equal("Error", current_mode_index);

    current_mode_index = 2;
    test_detect_assert_status_equal("Pending", current_mode_index);
    wait_for_timer_event();
    test_detect_assert_status_equal("Pass", current_mode_index);

    amxc_var_clean(&data_cb);
}


static int test_wan_setup(const char* wan_manager_odl) {
    amxd_object_t* root_obj = NULL;
    amxd_status_t rc = amxd_status_unknown_error;
    amxb_bus_ctx_t* bus_ctx = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    assert_int_equal(amxp_signal_new(NULL, &signal_handler, strsignal(SIGCHLD)), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "auto_sensing_toggle", AMXO_FUNC(_auto_sensing_toggle)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "toggle_mode", AMXO_FUNC(_toggle_mode)), 0);
    rc = amxo_parser_parse_file(&parser, odl_defs, root_obj);

    if(NULL != wan_manager_odl) {
        rc = amxo_parser_parse_file(&parser, wan_manager_odl, root_obj);
    }

    if(amxd_status_ok != rc) {
        printf("Parsing Error: %s\n", amxo_parser_get_message(&parser));
    }

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    assert_int_equal(amxo_connection_add(&parser,
                                         amxb_get_fd(bus_ctx),
                                         connection_read,
                                         "dummy:/tmp/dummy.sock",
                                         AMXO_BUS,
                                         bus_ctx)
                     , 0);
    assert_int_equal(amxb_register(bus_ctx, &dm), 0);

    _wan_auto_sensing_main(0, &dm, &parser);

    test_handle_events();

    return 0;
}

static void test_wan_autosensing_read_sigalrm(void) {
    sigset_t mask;
    int sfd;
    struct signalfd_siginfo fdsi;
    ssize_t s;

    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);

    sfd = signalfd(-1, &mask, 0);
    s = read(sfd, &fdsi, sizeof(struct signalfd_siginfo));
    assert_int_equal(s, sizeof(struct signalfd_siginfo));
    if(fdsi.ssi_signo == SIGALRM) {
        printf("Got SIGALRM\n");
    } else {
        printf("Read unexpected signal\n");
    }
}
static void wait_for_timer_event(void) {
    test_wan_autosensing_read_sigalrm();
    amxp_timers_calculate();
    amxp_timers_check();
}

static void test_wan_autosensing_set_enable(bool state) {
    amxd_dm_t* dm = test_get_dm();
    amxd_object_t* wan_autosensing = NULL;
    amxd_trans_t transaction;

    assert_non_null(dm);

    wan_autosensing = amxd_dm_findf(dm, "%sWANAutoSensing", test_get_prefix());
    assert_non_null(wan_autosensing);
    amxd_trans_init(&transaction);
    amxd_trans_select_object(&transaction, wan_autosensing);
    amxd_trans_set_value(bool, &transaction, "Enable", state);
    amxd_trans_apply(&transaction, dm);

    test_handle_events();

    amxd_trans_clean(&transaction);
}

static void test_wan_autosensing_assert_status_equal(const char* to) {
    amxd_dm_t* dm = test_get_dm();
    amxd_object_t* auto_sensing_plugin = NULL;
    char* status = NULL;
    assert_non_null(dm);

    auto_sensing_plugin = amxd_dm_findf(dm, "%sWANAutoSensing", test_get_prefix());
    assert_non_null(auto_sensing_plugin);

    status = amxd_object_get_value(cstring_t, auto_sensing_plugin, "Status", NULL);

    assert_non_null(status);
    assert_string_equal(to, status);

    free(status);
}

static void test_detect_assert_status_equal(const char* to, int index) {
    amxd_dm_t* dm = test_get_dm();
    amxd_object_t* detect = NULL;
    char* status = NULL;
    assert_non_null(dm);

    detect = amxd_dm_findf(dm, "%sWANAutoSensing.Detect.%d", test_get_prefix(), index);
    assert_non_null(detect);

    status = amxd_object_get_value(cstring_t, detect, "Status", NULL);

    assert_non_null(status);
    assert_string_equal(to, status);

    free(status);
}

int __wrap_amxb_call(UNUSED amxb_bus_ctx_t* const bus_ctx,
                     UNUSED const char* object,
                     const char* method,
                     UNUSED amxc_var_t* args,
                     amxc_var_t* ret,
                     UNUSED int timeout) {

    if(0 == strcmp(method, "setWANMode")) {
        amxc_var_t* status = NULL;
        amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
        status = amxc_var_add(amxc_htable_t, ret, NULL);
        amxc_var_add_key(bool, status, "status", true);
    } else if(0 == strcmp(method, "getCurrentWANModeStatus")) {
        amxc_var_t* status = NULL;
        amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
        status = amxc_var_add(amxc_htable_t, ret, NULL);
        amxc_var_add_key(bool, status, "status", current_mode_index == 2);
    }

    return 0;
}